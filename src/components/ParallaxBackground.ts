import Phaser, {Scene} from 'phaser';


interface ILayerConfig {
    id: string;
    offset: number;
}

interface ILayer extends ILayerConfig {
    item: Phaser.GameObjects.TileSprite;
}

export default class ParallaxBackground extends Phaser.GameObjects.Container {
    scene: Scene;
    layers: ILayer[];

    constructor(scene: Scene, layers: ILayerConfig[]) {
        super(scene);
        this.scene = scene;
        this.layers = this.createLayers(layers);

    }

    createLayer({id, offset}: ILayerConfig): ILayer {
        const item = new Phaser.GameObjects.TileSprite(this.scene, 0, 0, 0, 0, id).setOrigin(0);

        this.scene.add.existing(item);

        return {id, offset, item};
    }

    createLayers(layersConfig: ILayerConfig[]) {
        return layersConfig.map((config) => {
            return this.createLayer(config);
        });
    }

    move() {
        this.layers.forEach(({item, offset}) => {
            item.tilePositionY -= offset;
        });
    }
}
