import Phaser, {Scene} from 'phaser';
import BaseInteractiveUnit, {IBaseInteractiveUnit} from './BaseInteractiveUnit';
import EventDispatcher from '../libs/EventDispatcher';
import {
    EVENTS,
    HAZARD_POINTS_TEXT_CONFIG,
} from '../constants/game';


export interface IHazard extends IBaseInteractiveUnit {
    points: number;
}

const POINT_OFFSET = 5;

// TODO: тут будут находиться общие методы для всех типов врагов
export default class Hazard extends BaseInteractiveUnit {
    emitter: EventDispatcher;
    points: number;

    constructor(scene: Scene, config: IHazard) {
        const {
            x, y, texture, speed,
            health, layer, frame,
            points,
        } = config;

        super(scene, {x, y, texture, speed, health, layer, frame});

        this.emitter = new EventDispatcher();
        this.points = points;
    }

    showPoints() {
        this.emitter.emit(EVENTS.SET_SCORE, this.points);

        const points = this.scene.add.text(
            this.x,
            this.y - this.height / 2,
            String(this.points),
            HAZARD_POINTS_TEXT_CONFIG,
        )
            .setOrigin(0.5, 0);

        this.scene.tweens.add({
            targets: points,
            y: points.y - POINT_OFFSET,
            ease: 'Linear',
            duration: 350,
            onComplete: () => {
                this.scene.tweens.add({
                    targets: points,
                    alpha: 0,
                    ease: 'Linear',
                    duration: 250,
                    onComplete: () => {
                        points.destroy();
                    },
                });
            },
        });
    }

    explode() {
        super.explode();

        this.showPoints();

        this.setOrigin(0.5, 0.5);
        this.once(Phaser.Animations.Events.SPRITE_ANIMATION_COMPLETE, () => {
            // Без этого костыля destroy вызывается раньше, чем заканчивается анимация
            const intervalId = setInterval(() => {
                if (!this.anims.isPlaying) {
                    clearInterval(intervalId);
                    this.destroy();
                }
            }, 500);
        }, this);
        this.play('unit_explode');
    }
}
