import Phaser, {Scene} from 'phaser';
import {Layers} from '../typings/game';
import {BLINK_DURATION} from '../constants/game';


export interface IBaseInteractiveUnit {
    x: number;
    y: number;
    speed: number;
    health: number;
    texture: string;
    layer: Layers;
    frame?: string;
}

export default class BaseInteractiveUnit extends Phaser.GameObjects.Sprite {
    layer: Layers;
    speed: number;
    health: number;

    constructor(scene: Scene, config: IBaseInteractiveUnit) {
        const {
            x, y, texture, speed,
            health, layer, frame,
        } = config;

        super(scene, x, y, texture, frame);

        this.layer = layer;
        this.speed = speed;
        this.health = health;

        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.body.enable = true;
    }

    move() {
        this.body.setVelocityY(this.speed);
    }

    hurt(damage: number) {
        this.health -= damage;

        if (this.health <= 0) {
            this.explode();

            return;
        }

        this.setPipeline('blink');
        setTimeout(() => this.resetPipeline(), BLINK_DURATION);
    }

    explode() {
        this.body.enable = false;
    }
}

