import Phaser, {Scene} from 'phaser';
import BaseUIUnit from './BaseUIUnit';
import {
    ALIGN,
    EVENTS,
    UI_ALPHA,
    UI_ALPHA_ACTIVE,
} from '../../constants/game';


export default class XButton extends BaseUIUnit {
    constructor(scene: Scene) {
        super({
            scene,
            align: `${ALIGN.RIGHT}-${ALIGN.BOTTOM}`,
            offsetX: 30,
            offsetY: 30,
            texture: 'x_button',
            frame: 'x_button_idle',
        });

        this.setEvents();
    }

    setEvents() {
        this.setInteractive()
            .on(Phaser.Input.Events.POINTER_DOWN, () => {
                this.setFrame('x_button_pressed');
                this.setAlpha(UI_ALPHA_ACTIVE);
                this.emitter.emit(EVENTS.PRESS_X_BUTTON, {pressed: true});
            })
            .on(Phaser.Input.Events.POINTER_UP, () => {
                this.setFrame('x_button_idle');
                this.setAlpha(UI_ALPHA);
                this.emitter.emit(EVENTS.PRESS_X_BUTTON, {pressed: false});
            });
    }
}
